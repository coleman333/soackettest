const http = require('http');
const socketIo = require('socket.io');
const fs = require('fs');
const path = require('path');
const _=require('lodash');
const util =require('util');

const server = http.createServer((req, res, next) => {
	const data = fs.readFileSync(path.join(__dirname, 'index.html'));
	res.end(data)
});
server.listen(3007);

const io = socketIo(server);
// const sockets={}
io.on('connection', async function (socket) {
	//io.sockets
	// sockets[userId]=socket;
	const ids=await util.promisify(io.sockets.clients).call(io.sockets);
	console.log(`New user connected - ${_.size(ids)}`);

		// socket.emit()
// const s =		io.sockets.find(s=>s.id===1);
// s.emit()

	socket.on('disconnect', async reason => {
		const ids=await util.promisify(io.sockets.clients).call(io.sockets);
		console.log(`Socket disconnected - ${reason} - ${_.size(ids)}`)
	})

	socket.on('message', data => {
		const {userName, message} = data;
		io.sockets.emit('message1', `${userName}: ${message}`);
	});

	io.sockets.emit('message1', `New user connected`);
});

